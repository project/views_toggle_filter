<?php
/**
 * @file
 * Views Toggle Filter module hook implementations.
 */

/**
 * Implements hook_views_api().
 */
function views_toggle_filter_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'views_toggle_filter') . '/views',
  );
}

/**
 * After build form processor for the views exposed form.
 */
function views_toggle_filter_exposed_form_after_build($form, $form_state) {
  if (!isset($form_state['views_toggle_filter'])) {
    return $form;
  }

  foreach ($form_state['views_toggle_filter'] as $identifier => $info) {
    foreach ($info as $toggle_filter_value => $item) {
      foreach (array_keys($item) as $dependent_identifier) {
        if (!isset($form[$dependent_identifier])) {
          continue;
        }
        $element = &$form[$dependent_identifier];
        if (!isset($element['#id'])) {
          continue;
        }
        views_toggle_filter_set_element_dependency($element, $identifier, $toggle_filter_value);
      }
    }
  }

  return $form;
}

function views_toggle_filter_set_element_dependency(&$element, $identifier, $value) {
  if (!isset($element['#dependency'])) {
    $element['#dependency'] = array();
  }
  $element['#dependency']['radio:' . $identifier] = array($value);

  if (!isset($element['#dependency_count'])) {
    $element['#dependency_count'] = 1;
  }
  else {
    $element['#dependency_count']++;
  }
  if (!isset($element['#dependency_type'])) {
    $element['#dependency_type'] = 'hide';
  }

  if ((!isset($element['#pre_render'])) || (!in_array('ctools_dependent_pre_render', $element['#pre_render']))) {
    // Non-standard element, attach dependent.js manually.
    // Do not rely on ctools_dependent_pre_render() since it uses $element['#type'] which can be absent.
    $element['#attached']['js'][] = ctools_attach_js('dependent');
    $options = array();
    $options['CTools']['dependent'][$element['#id']] = array(
      'values' => $element['#dependency'],
      'num' => $element['#dependency_count'],
      'type' => $element['#dependency_type'],
    );
    $element['#attached']['js'][] = array('type' => 'setting', 'data' => $options);
  }
}
